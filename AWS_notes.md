
==AWS Notes==

:star2:   :star:   :star2:



=======
# AWS Notes
**FredSalianga** 
I learned that VS code can help you upload images to a repository and I explored the learning material in the skill builder and I had a lot of key takeaways.
**AWS Aurora**: is a MySQL and PostgreSQL compatible relational database engine that combines the speed and availability of high-end commercial databases with the simplicity and cost-eﬀectiveness of open source databases.
**Amazon DynamoDB**: is a key-value and document database that delivers 
single-digit millisecond performance at any scale.
**AWS Elasticache**: is a web service that makes it easy to deploy, operate, and scale an in-memory cache in the cloud.
I also reviewed my terminal for the git commands to allow me to be more comfortable with git.
I also learned about the data encryption.currently trying to understand a password cracking system: John the ripper from kali linux.
![AWS](images/AWS-databases1.png)

### Week 8
we proceed with finishing our security modules and we completed our presentation about brutal force attack focalizing on **"John the ripper"** and the group was successful in finding mutiple ways of doing it.
we look foward to the **Python** week to start. 

### Below I have attached screenshots of the work I did in the previuos weeks
- learned how to use writting syntax and getting better at it.
* The group was able to present on the security modules
+ Now we will be learning the AWS CLI (JAWS) 

![AWS6](images/Johntheripper.png)
![AWS7](images/eaglecollage.png)
![AWS8](images/kali.png)
![AWS9](images/kali2.png)

##Next couple weeks learning piorities!!
- git commit should be a habit 
* Watch Stephen Mareek AWS videos on Udemy to be more practical and understanding the AWS console
+ Prepare for the AWS CLP exam 

## Week 10.
finished week 9 and started week 10 where we are currently working on the JAWS modules and we had a very interresting lab which we had to create a vpc and test if the http traffic was allowed by modifying a html file during this lab we had the chance to work with the other groups.
![AWS2](images/Lab172.png)

### Key-points.
+ Exams test practices everyday 
* Git commits are essential
- Drink water :)
+ Myles pointed that AWS cloud policies questions are tricky
- Python is on the way ;)
* Finish Jaws modules



#Sofnas Notes

**What is cloud computing?**


Cloud computing is the on-demand delivery of compute power, database, storage, applications, and other IT resources through a cloud services platform via the internet with pay-as-you-go pricing. Whether you are running applications that share photos to millions of mobile users or you’re supporting the critical operations of your business, a cloud services platform provides rapid access to flexible and low-cost IT resources. With cloud computing, you don’t need to make large upfront investments in hardware and spend a lot of time on the heavy lifting of managing that hardware. Instead, you can provision exactly the right type and size of computing resources you need to power your newest bright idea or operate your IT department. You can access as many resources as you need, almost instantly, and only pay for what you use.

Cloud computing provides a simple way to access servers, storage, databases and a broad set of application services over the internet. A cloud services platform such as Amazon Web Services owns and maintains the network-connected hardware required for these application services, while you provision and use what you need via a web application.



add image ![AWS3](images/Dictionary Attack.PNG)
AWS services and features
Analytics:
<dl>
<dt>AWS SERVICES</dt>
<dt>ANALYTICS</dt>

<dd> - [1] Amazon Athena</dt>
<dd> - [2] Amazon Kinesis</dt>
<dd> - [3] Amazon QuickSight</dt>
<dt> Application Integration:</dt>
<dd> - [5] Amazon Simple Notification Service (Amazon SNS)</dt>
<dd> - [6] Amazon Simple Queue Service (Amazon SQS)</dt>
<dt> Compute and Serverless:</dt>
<dd> - [7]  AWS Bath</dt>
<dd> - [8] Amazon EC2</dt>
<dd> - [9] AWS Elastic Beanstalk</dt>
<dd> - [10]  AWS Lambda</dt>
<dd> - [11]  Amazon Lightsail</dt>
<dd> - [12] Amazon WorkSpaces</dt>
<dt>Containers:</dt>
<dd> - [12]  Amazon Elastic Container Service (Amazon ECS)</dt>
<dd> - [13]  Amazon Elastic Kubernetes Service (Amazon EKS)</dt>
<dd> - [14] AWS Fargate</dt>
<dt>Database:</dt>
<dd> - [15] Amazon Aurora</dt>
<dd> - [16] Amazon DynamoDB</dt>
<dd> - [17] Amazon ElastiCache</dt>
<dd> - [18]  Amazon RDS</dt>
<dd> - [19]  Amazon Redshift</dt>
<dt>Developer Tools:</dt>
<dd> - [20]  AWS CodeBuild</dt>
<dd> - [21]  AWS CodeCommit</dt>
<dd> - [22]  AWS CodeDeploy</dt>
<dd> - [23]  AWS CodePipeline</dt>
<dd> - [24]  AWS CodeStar</dt>
<dt>Customer Engagement:</dt>
<dd> - [25]  Amazon Connect</dt>
<dt>Management, Monitoring, and Governance:</dt>
<dd> - [26]  AWS Auto Scaling</dt>
<dd> - [27]  AWS Budgets</dt>
<dd> - [28]  AWS CloudFormation</dt>
<dd> - [29]  AWS CloudTrail</dt>
<dd> - [30]  CloudWatch</dt>
<dd> - [31]  Config</dt>
<dd> - [32]  AWS Cost and Usage Report</dt>
<dd> - [33]  Amazon EventBridge (Amazon CloudWatch Events)</dt>
<dd> - [34]  AWS License Manager</dt>
<dd> - [35]  AWS Managed Services</dt>
<dd> - [36]  AWS Organizations</dt>
<dd> - [37] AWS Secrets Manager</dt>
<dd> - [38] AWS Systems Manager</dt>
<dd> - [39]  AWS Systems Manager Parameter Store</dt>
<dd> - [40]  AWS Trusted Advisor</dt>
<dt>Networking and Content Delivery:</dt>
<dd> - [41] Amazon API Gateway</dt>
<dd> - [42]  Amazon CloudFront</dt>
<dd> - [43]  AWS Direct Connect</dt>
<dd> - [44] Amazon Route 53</dt>
<dd> - [45]  Amazon VPC</dt>
<dt>Security, Identity, and Compliance:</dt>
<dd> - [46]  AWS Artifact</dt>
<dd> - [47] AWS Certificate Manager (ACM)</dt>
<dd> - [48] AWS CloudHSM</dt>
<dd> - [49] Amazon Cognito</dt>
<dd> - [50] Amazon Detective</dt>
<dd> - [51] Amazon GuardDuty</dt>
<dd> - [52] AWS Identity and Access Management (IAM)</dt>
<dd> - [53] Amazon Inspector</dt>
<dd> - [54] AWS License Manager</dt>
<dd> - [55] Amazon Macie</dt>
<dd> - [56] AWS Shield</dt>
<dd> - [57] AWS WAF</dt>
<dt>Storage:</dt>
<dd> - [58] AWS Backup</dt>
<dd> - [59] Amazon Elastic Block Store (Amazon EBS)</dt>
<dd> - [60]  Amazon Elastic File System (Amazon EFS)</dt>
<dd> - [61]  Amazon S3</dt>
<dd> - [62]  Amazon S3 Glacier</dt>
<dd> - [63]  AWS Snowball Edge</dt>
<dd> - [64]  AWS Storage Gateway</dt>
</dl>


**Amazon FSx – Overview**

Launch 3rd party high-performance file systems on AWS
• Fully managed service
![AWSCLOUD](images/AWSCLOUD.jpg)
![AWSCLOUD](images/AWSCLOUD1.jpg)



**Global Accelerator**
	AWS Global Accelerator is a service that improves the availability and performance of your applications with local or global users. It provides static IP addresses that act as a fixed entry point to your application endpoints in a single or multiple AWS Regions, such as your Application Load Balancers, Network Load Balancers, or Amazon EC2 instances. AWS Global Accelerator uses the AWS global network to optimize the path from your users to your applications, improving the performance of your traffic by as much as 60%.
	Global Accelerator improves performance for a wide range of applications over TCP or UDP by proxying packets at the edge to applications running in one or more AWS Regions. Global Accelerator is a good fit for non-HTTP use cases, such as gaming (UDP), IoT (MQTT), or Voice over IP, as well as for HTTP use cases that specifically require static IP addresses or deterministic, fast regional failover.


**AWS Config** 
 AWS Config is a service that enables you to assess, audit, and evaluate the configurations of your AWS resources. Config continuously monitors and records your AWS resource configurations and allows you to automate the evaluation of recorded configurations against desired configurations. Think resource-specific change history, audit, and compliance; think Config. Its a configuration tracking service and not an infrastructure tracking service.

**Amazon CloudWatch** 
	Amazon CloudWatch is a monitoring and observability service built for DevOps engineers, developers, site reliability engineers (SREs), and IT managers. CloudWatch provides data and actionable insights to monitor applications, respond to system-wide performance changes, optimize resource utilization, and get a unified view of operational health. Amazon EBS emits notifications based on Amazon CloudWatch Events for a variety of volume, snapshot, and encryption status changes. With CloudWatch Events, you can establish rules that trigger programmatic actions in response to a change in volume, snapshot, or encryption key state (though not for underutilized volume usage).

**AWS Glacier**

![AWS1](images/AwsGlacier.png)




#Mariya
![AWS2](images/PGP.png)






![AWSLab](images/lab.png)

#Marco
I will be focusing on AWS IAM and VPC services. I will begin to gather note and resources that can help anyone prepare for thier AWS certification.

IAM
AWS IAM is the tool used to assign groups/roles/access to services throughout the AWS console. IAM can assign special permissions to users if granted by administrators of the account

VPC
AWS VPC is a virtual private cloud service found within the console. With VPC you can set layers of security to help your account stay safe from outside attacks and outsiders that do not have clearance inside. They are different layers to this tool to explore but we will start with the outer most layer which is the internet gateway.
